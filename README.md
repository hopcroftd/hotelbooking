# README #

## Test requirements:
  - Oracle Java 1.8.0_291 (or higher)
  - Maven 3.6.3

## Test site
  URL = http://hotel-test.equalexperts.io/

## Project Documentation:
  URL = https://dhopcroft.atlassian.net/wiki/spaces/HB/pages/262529/Newsletter

## Project tracking
  URL = https://dhopcroft.atlassian.net/jira/software/c/projects/EET/issues/

## running the tests
  mvn test