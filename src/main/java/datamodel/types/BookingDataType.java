package datamodel.types;

/**
 * This enum defines the order to find the table data for booking date definition
 */
public enum BookingDataType {
    FIRSTNAME,
    SURNAME,
    COST,
    DEPOSITPAID,
    CHECKIN,
    CHECKOUT
}
