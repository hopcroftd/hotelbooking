package datamodel;

import datamodel.types.BookingDataType;

import java.util.List;
import java.util.Locale;


/**
 * data builder for booking information. this allows data to be created as;
 *  - a big constructor call,
 *  - from a list (see BookingDataType for order)
 *  - of as a builder
 */
public class Booking {
    private String firstname;
    private String surname;
    private String cost;
    private Boolean depositPaid;
    private String checkin;
    private String checkout;

    public Booking (String firstname,
                    String surname,
                    String cost,
                    Boolean depositPaid,
                    String checkin,
                    String checkout
    ) {
        this.firstname = firstname;
        this.surname = surname;
        this.cost = cost;
        this.depositPaid = depositPaid;
        this.checkin = checkin;
        this.checkout = checkout;
    }

    public Booking(List<String> bookingTable) {
        this.firstname = bookingTable.get(BookingDataType.FIRSTNAME.ordinal());
        this.surname = bookingTable.get(BookingDataType.SURNAME.ordinal());
        this.cost = bookingTable.get(BookingDataType.COST.ordinal());
        String deposit = bookingTable.get(BookingDataType.DEPOSITPAID.ordinal());
        boolean depositPaid = deposit.toLowerCase(Locale.ROOT).equals("true") ? true : false;
        this.depositPaid = depositPaid;
        this.checkin = bookingTable.get(BookingDataType.CHECKIN.ordinal());
        this.checkout = bookingTable.get(BookingDataType.CHECKOUT.ordinal());
    }

    public String getFirstname() {
        return this.firstname;
    }

    public String getSurname() {
        return this.surname;
    }

    public String getCost() {
        return this.cost;
    }

    public boolean getDepositPaid() {
        return this.depositPaid;
    }

    public String getCheckin() {
        return this.checkin;
    }

    public String getCheckout() {
        return this.checkout;
    }

    @Override
    public String toString() {
        return (this.firstname!=null ? (this.firstname + "\n") : "")
                + (this.firstname!=null ? (this.firstname + "\n") : "")
                + (this.surname!=null ? (this.surname + "\n") : "")
                + (this.cost!=null ? (this.cost + "\n") : "")
                + (this.depositPaid!=null ? (this.depositPaid.toString() + "\n") : "")
                + (this.checkin!=null ? (this.checkin + "\n") : "")
                + (this.checkout!=null ? (this.checkout + "\n") : "");
    }

    public static class BookingBuilder {
        private String builderFirstname;
        private String builderSurname;
        private String builderCost;
        private Boolean builderDepositPaid;
        private String builderCheckin;
        private String builderCheckout;

        public BookingBuilder() {
        }

        public BookingBuilder firstname(String theFirstname) {
            this.builderFirstname = theFirstname;
            return this;
        }

        public BookingBuilder surname(String theSurname) {
            this.builderSurname = theSurname;
            return this;
        }

        public BookingBuilder cost(String theCost) {
            this.builderCost = theCost;
            return this;
        }

        public BookingBuilder depositPaid(boolean hasPaid) {
            this.builderDepositPaid = hasPaid;
            return this;
        }

        public BookingBuilder checkin(String theCheckin) {
            this.builderCheckin = theCheckin;
            return this;
        }

        public BookingBuilder checkout(String theCheckout) {
            this.builderCheckout = theCheckout;
            return this;
        }

        public Booking create() {
            return new Booking(
                    builderFirstname,
                    builderSurname,
                    builderCost,
                    builderDepositPaid,
                    builderCheckin,
                    builderCheckout
            );
        }
    }
}
