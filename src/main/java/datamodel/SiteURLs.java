package datamodel;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.swing.plaf.synth.SynthEditorPaneUI;
import java.nio.file.Files;
import java.io.IOException;
import java.nio.file.Paths;

public class SiteURLs {
    private static JSONArray site_data;
    JSONParser parser = new JSONParser();

    public SiteURLs() throws IOException, ParseException {
        String content = new String(Files.readAllBytes(Paths.get("./src/main/java/resources/data/sites.json")));
        site_data = (JSONArray) parser.parse(content);
    }

    public static String getSite(String record) {
        JSONObject urlEntry = (JSONObject) site_data.get(0);
        return (String) urlEntry.get(record);
    }
}


