package Pages;

import datamodel.Booking;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;


public class BookingForm {
    WebDriver driver;

    @FindBy(how= How.XPATH, using="/html/body/div[1]/div[1]/h1")
    private WebElement pageHeading;

    @FindBy(how= How.ID, using="firstname")
    private WebElement firstname;

    @FindBy(how= How.ID, using="lastname")
    private WebElement surname;

    @FindBy(how= How.ID, using="totalprice")
    private WebElement price;

    @FindBy(how= How.ID, using="depositpaid")
    private WebElement deposit;

    @FindBy(how= How.ID, using="checkin")
    private WebElement checkin;

    @FindBy(how= How.ID, using="checkout")
    private WebElement checkout;

    @FindBy(how= How.XPATH, using="//*[@id=\"form\"]/div/div[7]/input")
    private WebElement savebutton;

    public BookingForm(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements((org.openqa.selenium.WebDriver) this.driver,this);
    }

    public void enterFirstName(String name) {
        firstname.sendKeys(name);
    }
    public String getFirstName() { return firstname.getText(); }

    public void enterSurname(String name) {
        surname.sendKeys(name);
    }

    public void enterPrice(String cost) { price.sendKeys(cost); }

    public void selectDepositPaid(Boolean state) {

        Select depositpaid = new Select(deposit);
        if (state==true) {
            depositpaid.selectByVisibleText("true");
        } else {
            depositpaid.selectByVisibleText("false");
        }
    }

    public void enterCheckinDate(String date) { checkin.sendKeys(date); }

    public void enterCheckoutDate(String date){ checkout.sendKeys(date); }

    public void clickSaveBooking() {
        savebutton.click();
    }

    public void enterBooking(Booking booking) {
        enterFirstName(booking.getFirstname());
        enterSurname(booking.getSurname());
        enterPrice(booking.getCost());
        selectDepositPaid(true);
        enterCheckinDate(booking.getCheckin());
        enterCheckoutDate(booking.getCheckout());
        clickSaveBooking();
    }

    public void enterBookingData() {

    }

    /**
     * As there is no form validation I check to see if form entry fields still have entred data
     * @return  True if entry fields are blank, false if they have data in them
     */
    public boolean checkEntryBlank() {
        return false;
    }

    /**
     * Loop over booking form to find given test data
     * @return true if entry found or false if it was not found
     */
    public boolean checkBooking() {
        return false;
    }

}
