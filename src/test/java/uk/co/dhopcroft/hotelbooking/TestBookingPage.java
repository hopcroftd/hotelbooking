package uk.co.dhopcroft.hotelbooking;

import Pages.BookingForm;
import Support.BrowserInstance;
import Support.SeleniumPropertySetup;
import datamodel.Booking;
import datamodel.SiteURLs;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

public class TestBookingPage {
    private static String url;
    private static WebDriver wd;

    @BeforeAll
    public static void setUp() {
        new SeleniumPropertySetup("3.141.59", false);
        try {
            SiteURLs url = new SiteURLs();
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        url = SiteURLs.getSite("booking-test");
        wd = (WebDriver) new BrowserInstance("firefox", true).getBrowserDriver();
    }

    @AfterAll
    public static void tearDown() {
        wd.quit();
    }

    @Test
    public void testValidDataCanBeEntered() throws InterruptedException {
        wd.get(url);
        BookingForm bookingFormPage = new BookingForm(wd);
        bookingFormPage.enterFirstName("William");
        bookingFormPage.enterSurname("Tell");
        bookingFormPage.enterPrice("123.45");
        bookingFormPage.selectDepositPaid(false);
        Thread.sleep(500);  // This selection takes time
        // Enter dates 1 and 2 days from now
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String start, end;
        start = LocalDate.now().plusDays(1).toString();
        bookingFormPage.enterCheckinDate(start);
        end = LocalDate.now().plusDays(2).toString();
        bookingFormPage.enterCheckoutDate(end);
        // Submit booking
        bookingFormPage.clickSaveBooking();
        // Check that input text boxes have been cleared
        String name = bookingFormPage.getFirstName();
        assertTrue(name.equals(""));
    }
}
