package Support.POM;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class BasePageElement {
    By locator;

    protected static final Logger log = Logger.getLogger(BasePageElement.class);

    WebDriver browser;

    public BasePageElement(WebDriver browser, By locatorType) {
        this.browser = browser;
        this.locator = locatorType;
    }
}
