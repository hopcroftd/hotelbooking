package Support;

import org.apache.commons.lang3.SystemUtils;


public class SeleniumPropertySetup {
    public SeleniumPropertySetup(String version, boolean prefer64bits) {
        String os;
        // Determine the OS type, so the correct driver path can be determined
        // if not Windows, Mac or Linux then exception thrown.
        if (SystemUtils.IS_OS_WINDOWS) { os = "win"; }
        else if (SystemUtils.IS_OS_MAC_OSX) {
            os = "mac";
            // Mac only has 64 bit version
            prefer64bits = true;
        }
        else if (SystemUtils.IS_OS_LINUX) { os = "linux"; }
        else { throw new IllegalStateException("Operating System" + System.getProperty("os.name") + "not supported"); }
        // Application Directories
        String basedir = System.getProperty("user.dir") + "/src/main/java/resources/drivers/se/" + version + "/" + os + "/";
        System.setProperty("user.appdata.driver.base", basedir);
        // Webdriver locations
        String geckoDriver = basedir + "Gecko_" + (prefer64bits?"64":"32") + "/geckodriver" + (SystemUtils.IS_OS_WINDOWS?".exe":"");
        System.setProperty("webdriver.gecko.driver", geckoDriver);
        // System output locations
        System.setProperty("user.appdata.evidence", "./evidence/");
    }
}
